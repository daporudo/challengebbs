require 'sinatra'
require 'sinatra/reloader'
require 'mongo'

before do
  db_con  = Mongo::Connection.new('localhost')
  @db     = db_con.db('bbs')
  @bbs   = @db.collection('bbs')
end

helpers do
  # htmlタグエスケープのエイリアスメソッド
  include Rack::Utils
  alias_method :esc, :escape_html
end

get '/' do
  erb :index
end

post '/new' do
  # コメントが空白か判定
  if !params[:comment].empty? then
    dateobj = DateTime.now
    date = dateobj.strftime("%Y/%m/%d %H:%M:%S")

    # 名前が空白なら"No Name"に
    if params[:name].empty? then
      params[:name] = "No name"
    end

    @bbs.insert(:name => params[:name], :comments => params[:comment], :date => "#{date}")
    redirect '/'
  else
    @error = "コメントが入力されていません"
    erb :index
  end
end
